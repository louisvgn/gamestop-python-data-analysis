# GameStop Python Data Analysis



## What is it?

As part of a research project in a sociology class, I conducted for my group a quantitative analysis based on the data available as datasets on Kaggle (more details in the main files).

I figure I could use this project as a pretext to learn more about python and data analysis in the end. Hence, the project was used as a *self-taught introduction* to python data analysis.

I haven’t had the time yet to really clean up the main file (the [Python_Analysis.ipynb](/Python_Analysis.ipynb), the `.py` is just a straight download from google collaboratory to make it easier to run without jupyter installed) by removing unuseful comments, cells that just save files or read files from `/data`[^1] as some functions took too much time to call multiple times (*i.e.*, pre-processing and topic modeling).


[^1]: This is where the data sets were supposed to be, but they are too big to include in this repo. The links to them can be found in the `Python_Analysis.ipynb` file’s introduction.

I don’t know if I’ll ever do it. If you come across this repo and are curious, don’t hesitate to reach out. **If you are one of those much too kind people and want to give a hand**, there are tons of things that need improvement for which I was not able to find a correct solution:
* The pre-processing function takes too long. Is there any way to optmize it?
* I haven’t been able to properly process the "I" pronoun, which we wanted to keep for our analysis. I don’t know why, but it keep being removed. 
* Any optimization on the topic modeling function?
* Is it really a good idea to call functions within functions? It’s the way I code the main function to plot the evolution of themes overtime. I’m no engineer, so it may not be the smartest way to do it.
* I really couldn’t figure out why I was not able to plot the distribution of scores… I don’t know what I’m doing wrong. I opened a question on [stackoverflow](https://stackoverflow.com/questions/77645510/simple-seaborn-distribution-plot-not-working)


## Licence 

If you’re a student (or really anyone) and want to reuse part of the code, feel free to do so under the MIT licence. Bear in mind, if you don’t really understand what you’re doing that above &uarr; are the main limitations of the code, and should be used cautiously to do any kind of analysis.

 [This work (anything other than code)](https://gitlab.com/louisvgn/gamestop-python-data-analysis/-/tree/main?ref_type=heads) © 2023 by Louis Vigneras is licensed under [Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1).
